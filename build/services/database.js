"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const index_1 = tslib_1.__importDefault(require("../models/index"));
module.exports = {
    name: "action-log.database",
    version: 4,
    settings: {
        $secureSettings: ["db"],
        db: {
            connectionString: process.env.DB_CONNECTION_STRING
        }
    },
    created() {
        if (!this.settings.db.connectionString)
            this.broker.fatal("Database connection string not supplied! Make sure DB_CONNECTION_STRING is populated!");
        const gs = index_1.default(this.settings.db.connectionString);
        // This should configure a process-wide Sequelize object
        this.db = gs.db;
        this.models = gs.models;
    }
};
