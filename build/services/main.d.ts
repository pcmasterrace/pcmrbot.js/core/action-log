import { Service, Context } from "moleculer";
import { Sequelize } from "sequelize";
import { ModelsInterface } from "../models/index";
import { Typings } from "@pcmrbotjs/core-typings";
export default class ActionLogService extends Service {
    db: Sequelize;
    models: ModelsInterface;
    constructor(broker: any);
    logAction(ctx: Context<{
        postName: string;
        service: string;
        actionTaken: string;
        timestamp: Date;
        logMessage: string;
    }, any>): Promise<void>;
    generateSlackBlocks(ctx: Context<{
        name: string;
    }>): Promise<unknown>;
    generateSlackBlocksComment(ctx: Context<Typings.Reddit.Comment>): Promise<any[]>;
    generateSlackBlocksSubmission(ctx: Context<Typings.Reddit.Submission>): Promise<any[]>;
    serviceCreated(): void;
}
