"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionLogService = void 0;
const tslib_1 = require("tslib");
const main_1 = tslib_1.__importDefault(require("./services/main"));
exports.ActionLogService = main_1.default;
function registerAllActionLogServices(broker) {
    broker.createService(main_1.default);
}
exports.default = registerAllActionLogServices;
