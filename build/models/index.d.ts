import GlobalSequelize from "@pcmrbotjs/global-sequelize";
import { Model } from "sequelize";
import { ActionLogEntry } from "./logEntry";
export default function initDb(connectionString: string): GlobalSequelize;
export interface ModelsInterface {
    ActionLogEntry?: typeof ActionLogEntry;
    [key: string]: typeof Model;
}
export { ActionLogEntry };
