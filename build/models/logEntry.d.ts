import { Sequelize, Model, ModelAttributes, InitOptions } from "sequelize";
export declare class ActionLogEntry extends Model {
    id: number;
    postName: string;
    service: string;
    action: string;
    timestamp: Date;
}
export declare const ActionLogEntryAttributes: ModelAttributes;
export declare function ActionLogEntryOptions(sequelize: Sequelize): InitOptions;
