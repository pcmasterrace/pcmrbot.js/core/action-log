"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionLogEntry = void 0;
const tslib_1 = require("tslib");
const global_sequelize_1 = tslib_1.__importDefault(require("@pcmrbotjs/global-sequelize"));
const logEntry_1 = require("./logEntry");
Object.defineProperty(exports, "ActionLogEntry", { enumerable: true, get: function () { return logEntry_1.ActionLogEntry; } });
function initDb(connectionString) {
    const gs = new global_sequelize_1.default(connectionString);
    const registerLogEntry = gs.models.ActionLogEntry === undefined;
    // Initialize accounts
    if (registerLogEntry) {
        logEntry_1.ActionLogEntry.init(logEntry_1.ActionLogEntryAttributes, logEntry_1.ActionLogEntryOptions(gs.db));
        gs.models.ActionLogEntry = logEntry_1.ActionLogEntry;
    }
    // Establish associations
    // Synchronize models
    if (registerLogEntry) {
        gs.models.ActionLogEntry.sync({ alter: true });
    }
    return gs;
}
exports.default = initDb;
