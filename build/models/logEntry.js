"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionLogEntryOptions = exports.ActionLogEntryAttributes = exports.ActionLogEntry = void 0;
const sequelize_1 = require("sequelize");
class ActionLogEntry extends sequelize_1.Model {
}
exports.ActionLogEntry = ActionLogEntry;
exports.ActionLogEntryAttributes = {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    postName: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    service: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    action: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    timestamp: {
        type: sequelize_1.DataTypes.DATE,
        allowNull: false,
        defaultValue: () => new Date()
    }
};
function ActionLogEntryOptions(sequelize) {
    return {
        sequelize,
        tableName: "actionlog_Entry"
    };
}
exports.ActionLogEntryOptions = ActionLogEntryOptions;
