import { ServiceBroker } from "moleculer";
import ActionLogService from "./services/main";
export default function registerAllActionLogServices(broker: ServiceBroker): void;
export { ActionLogService };
