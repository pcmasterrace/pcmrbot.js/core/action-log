import { Service, Context, Errors } from "moleculer";
import { Sequelize, Op } from "sequelize";
import moment from "moment-timezone";

const ActionLogDatabase = require("./database");
import { ModelsInterface, ActionLogEntry } from "../models/index";

import { FastestValidator, Typings } from "@pcmrbotjs/core-typings";

export default class ActionLogService extends Service {
    public db: Sequelize;
    public models: ModelsInterface;

    constructor(broker) {
        super(broker);

        this.parseServiceSchema({
            name: "action-log",
            version: 4,
            mixins: [ ActionLogDatabase ],
            dependencies: [
                { name: "api.reddit.post", version: 4 },
                { name: "api.slack.web", version: 4 },
                { name: "api.slack.utilities", version: 4 }
            ],
            settings: {
                channel: process.env.ACTIONLOG_CHANNEL
            },
            actions: {
                logAction: {
                    name: "log",
                    params: {
                        postName: {
                            type: "string",
                            empty: false,
                            trim: true,
                            convert: true
                        },
                        service: {
                            type: "string",
                            empty: false,
                            trim: true,
                            convert: true
                        },
                        actionTaken: {
                            type: "string",
                            empty: false,
                            trim: true,
                            convert: true
                        },
                        timestamp: {
                            type: "date",
                            convert: true
                        },
                        logMessage: {
                            type: "string",
                            convert: true
                        }
                    },
                    handler: this.logAction
                },
                generateSlackBlocks: {
                    name: "slack.generate",
                    params: {
                        name: {
                            type: "string",
                            empty: false
                        }
                    },
                    handler: this.generateSlackBlocks
                },
                generateSlackBlocksComment: {
                    name: "slack.generate.t1",
                    params: FastestValidator.Reddit.Comment,
                    handler: this.generateSlackBlocksComment
                },
                generateSlackBlocksSubmission: {
                    name: "slack.generate.t3",
                    params: FastestValidator.Reddit.Submission,
                    handler: this.generateSlackBlocksSubmission
                }
            },
            created: this.serviceCreated
        });
    }

    async logAction(ctx: Context<{
        postName: string,
        service: string,
        actionTaken: string,
        timestamp: Date,
        logMessage: string
    }, any>) {
        // Log entry to database
        await this.models.ActionLogEntry.create({
            postName: ctx.params.postName,
            service: ctx.params.service,
            action: ctx.params.actionTaken
        });

        // Get post from Reddit
        let post: Array<Typings.Reddit.Comment | Typings.Reddit.Submission> = await ctx.call("v4.api.reddit.post.get.name", {
            names: ctx.params.postName
        });

        // Format log message
        let logMessage = ctx.params.logMessage;
        logMessage = logMessage.replace(/{author}/gi, post[0].author.username);
        logMessage = logMessage.replace(/{subreddit}/gi, post[0].subreddit);
        logMessage = logMessage.replace(/{kind}/gi, post[0].name.startsWith("t1") ? "comment" : "submission");
        logMessage = logMessage.replace(/{mod}/gi, post[0].removedBy as string);
        // @ts-ignore
        logMessage = logMessage.replace(/{title}/gi, post[0].name.startsWith("t1") ? post[0].parentSubmission.title : post[0].title);
        logMessage = logMessage.replace(/{url}/gi, `https://www.reddit.com${post[0].permalink}`);
        // @ts-ignore
        logMessage = logMessage.replace(/{link}/gi, post[0].name.startsWith("t1") ? undefined : post[0].url);
        logMessage = logMessage.replace(/{score}/gi, String(post[0].score));

        // Generate Slack blocks
        const blocks: any[] = await ctx.call("v4.action-log.slack.generate", post[0]);

        // Send Slack message
        await ctx.call("v4.api.slack.web.chat.post.message", {
            channel: this.settings.channel,
            text: logMessage,
            attachments: [{
                blocks
            }]
        });
    }

    async generateSlackBlocks(ctx: Context<{ name: string }>) {
        if (ctx.params.name.startsWith("t1_")) {
            return await ctx.call("v4.action-log.slack.generate.t1", ctx.params);
        } else if (ctx.params.name.startsWith("t3_")) {
            return await ctx.call("v4.action-log.slack.generate.t3", ctx.params);
        } else {
            throw new Errors.MoleculerClientError("Unsupported Reddit post type", 400, "INVALID_PARAM", {
                name: ctx.params.name
            });
        }
    }

    async generateSlackBlocksComment(ctx: Context<Typings.Reddit.Comment>) {
        let selfText: string = await ctx.call("v4.api.slack.utilities.markdown-to-mrkdwn", {
            text: ctx.params.body
        });
        let title: string = undefined;

        if (ctx.params.parentSubmission.title) {
            title = ctx.params.parentSubmission.title;
        } else {
            let parentSubmission: Typings.Reddit.Submission = await ctx.call("v4.api.reddit.post.get.name", {
                names: ctx.params.parentSubmission.name
            });

            title = parentSubmission[0].title;
        }

        const blocks: any[] = [
            {
                type: "section",
                text: {
                    type: "mrkdwn",
                    text: `[+${ctx.params.score}] [COMMENT] ` + 
                          `*<https://www.reddit.com${ctx.params.permalink}|${title}>*\n` +
                          `by <https://www.reddit.com/u/${ctx.params.author.username}|/u/${ctx.params.author.username}>`
                }
            },
            {
                type: "section",
                text: {
                    type: "mrkdwn",
                    text: selfText
                }
            },
            {
                type: "context",
                elements: [
                    {
                        type: "mrkdwn",
                        text: `/r/${ctx.params.subreddit}`
                    },
                    {
                        type: "mrkdwn",
                        text: `<!date^${moment.utc(ctx.params.created).unix()}^{date_short_pretty} at {time}|${moment.utc(ctx.params.created).format("LLL")} UTC>`
                    }
                ]
            }
        ]

        return blocks;
    }

    async generateSlackBlocksSubmission(ctx: Context<Typings.Reddit.Submission>) {
        let selfText: string = undefined;
        let imageLink: string = undefined;

        // GIFs should be animated on Slack. It's only right
        if (ctx.params.url && ctx.params.url.substr(-4, 4) === ".gif") {
            imageLink = ctx.params.url;
        } else if (ctx.params.url && (ctx.params.url.substr(-4, 4) === ".jpg" || ctx.params.url.substr(-4, 4) === ".png")) {
            imageLink = ctx.params.url;
        } else if (ctx.params.preview) {
            imageLink = ctx.params.preview.images[0].source.url;
        }

        if (ctx.params.body !== "") {
            selfText = await ctx.call("v4.api.slack.utilities.markdown-to-mrkdwn", {
                text: ctx.params.body
            });
        }

        const blocks: any[] = [
            {
                type: "section",
                text: {
                    type: "mrkdwn",
                    text: `[+${ctx.params.score}] [SUBMISSION] ` + 
                          `*<https://www.reddit.com${ctx.params.permalink}|${ctx.params.title}>*\n` +
                          `by <https://www.reddit.com/u/${ctx.params.author.username}|/u/${ctx.params.author.username}>`
                }
            }
        ]

        if (selfText) {
            blocks.push({
                type: "section",
                text: {
                    type: "mrkdwn",
                    text: selfText
                }
            });
        }
        if (imageLink) {
            blocks.push({
                type: "image",
                image_url: imageLink,
                alt_text: ctx.params.title
            });
        }

        blocks.push({
            type: "context",
            elements: [
                {
                    type: "mrkdwn",
                    text: `/r/${ctx.params.subreddit}`
                },
                {
                    type: "mrkdwn",
                    text: `<!date^${moment.utc(ctx.params.created).unix()}^{date_short_pretty} at {time}|${moment.utc(ctx.params.created).format("LLL")} UTC>`
                }
            ]
        });

        return blocks;
    }

    serviceCreated() {
        if (!this.settings.channel) this.broker.fatal("Action log Slack channel not supplied! Make sure ACTIONLOG_CHANNEL is populated!");
    }
}