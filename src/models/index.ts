import GlobalSequelize from "@pcmrbotjs/global-sequelize";
import { Model } from "sequelize";

import { ActionLogEntry, ActionLogEntryAttributes, ActionLogEntryOptions } from "./logEntry";

export default function initDb(connectionString: string): GlobalSequelize {
    const gs = new GlobalSequelize(connectionString);

    const registerLogEntry = gs.models.ActionLogEntry === undefined;

    // Initialize accounts
    if (registerLogEntry) {
        ActionLogEntry.init(ActionLogEntryAttributes, ActionLogEntryOptions(gs.db));
        gs.models.ActionLogEntry = ActionLogEntry;
    }

    // Establish associations


    // Synchronize models
    if (registerLogEntry) {
        gs.models.ActionLogEntry.sync({alter: true});
    }

    return gs;
}

export interface ModelsInterface {
    ActionLogEntry?: typeof ActionLogEntry,
    [key: string]: typeof Model;
}

export {
    ActionLogEntry
}