import { Sequelize, Model, DataTypes, ModelAttributes, InitOptions } from "sequelize";

export class ActionLogEntry extends Model {
    public id!: number;
    public postName!: string;
    public service!: string;
    public action!: string;
    public timestamp!: Date;
}

export const ActionLogEntryAttributes: ModelAttributes = {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    postName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    service: {
        type: DataTypes.STRING,
        allowNull: false
    },
    action: {
        type: DataTypes.STRING,
        allowNull: false
    },
    timestamp: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: () => new Date()
    }
}

export function ActionLogEntryOptions(sequelize: Sequelize): InitOptions {
    return {
        sequelize,
        tableName: "actionlog_Entry"
    }
}